///<reference types="Cypress"/>
Cypress.on('uncaught:exception',(err,runnable)=>{
    return false
})
describe('localhost test',()=>{
    it('Add new task test',()=>{
        cy.visit('http://localhost')
        cy.get('[href="/task/add"]').click()     
        cy.get('#summary').type('test docker')
        cy.get('#detail').type('first test')
        cy.get('#priority').select('Low')
        cy.get('#duedate').type('2563-01-14')
        cy.get('.btn').click() 
    })

    it('Task box check',()=>{
        cy.get('.card-header > .row > :nth-child(1)').should('be.visible')
        cy.get(':nth-child(2) > .badge').should('be.visible')
        cy.get('.card-title').should('be.visible')
        cy.get('.card-text').should('be.visible')
        cy.get('h4 > .badge').should('be.visible')
        cy.get(':nth-child(1) > .nav-link > .fas').should('be.visible')
        cy.get('[cl=""] > .nav-link > .fas').should('be.visible')
        cy.get('.btn > .fas').should('be.visible')
    })
    
    it('Edit test',()=>{
        cy.get(':nth-child(1) > .nav-link > .fas').click()
        cy.get('#priority').select('High')
        cy.get('.btn').click()
        cy.get(':nth-child(2) > .badge').should('contain.text','High')
    })

    it('Complete test',()=>{
        cy.get('.btn > .fas').click()
        cy.get('h4 > .badge').should('contain.text','Complete')
        cy.get('.btn > .fas').should('be.visible')
        cy.get('.btn > .fas').click()
        cy.get('.container').should('be.visible')
        cy.get('.btn > .fas').click()
    })

    it('Delete test',()=>{
        cy.get('.btn > .fas').should('be.visible')
        cy.get('.nav-link > .fas').click()
        cy.get('.btn-danger').should('be.visible')
        cy.get('.btn-danger').should('be.visible')
        cy.get('.btn-primary').click()
        cy.get('.card').should('be.visible')
        cy.get('.nav-link > .fas').click()
        cy.get('.btn-danger').click()
        cy.get('.container').should('be.visible')
    })
})